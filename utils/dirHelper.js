const fs = require('fs')
const mkdirp = require('mkdirp')

module.exports = {
    checkAndMkDir: (dir, callback) => {
        fs.exists(dir, function(exists) {
            if (!exists) {
                mkdirp(dir, (err) => {
                    if(err) {
                        console.log('Error in folder creation')
                        callback()
                    }  
                    callback()
                })
            } else {
                callback()
            }
        })
    }
}