module.exports = {
    autoFill: (text, char, length, prefix) => {
        const iPrefix = prefix || ''
        const round = length - text.length - iPrefix.length
        for (var i = 0; i < round; i++) {
            text = char + text
        }

        return iPrefix + text
    },
    findUserData: async (con, id, callback) => {
        return await con('admin')
            .select('*')
            .andWhere('id', id)
            .first()
            .asCallback(callback)
    }
}