const uuid = require('uuidv4').default
const bcrypt = require('bcrypt')
const sqlHelper = require('../utils/sqlHelper')
const dayjs = require('dayjs')

module.exports = {
    generateMemberForInsertToDb: async (params, number) => {
        const { 
            qrcodeSetId,
            qrcodeItemId,
            username,
            password,
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3,
            placeLatitude,
            placeLongitude,
            profileImageId,
            placeImageId } = params
        
        const privateKey = await bcrypt.genSalt(15)
        const encodePassword = await bcrypt.hash(password + privateKey, privateKey)

        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'MB'),
            number: number + 1,
            username: username,
            password: encodePassword,
            private_key: privateKey,
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            profile_image_id: profileImageId,
            place_image_id: placeImageId,
            qrcode_set_id: qrcodeSetId,
            qrcode_item_id: qrcodeItemId,
            place_latitude: placeLatitude,
            place_longitude: placeLongitude,
            status: 1
        }
    },
    generateMemberForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3,
            profileImageId,
            placeImageId,
            status } = params

        return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            profile_image_id: profileImageId,
            place_image_id: placeImageId,
            status: status,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberProfileForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3,
            profileImageId} = params
            
        return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            profile_image_id: profileImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberLocationForUpdateToDb: (params, user) => {
        const { 
            placeLatitude,
            placeLongitude,
            placeImageId} = params
            
        return {
            place_image_id: placeImageId,
            place_latitude: placeLatitude,
            place_longitude: placeLongitude,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberChangePasswordForUpdateToDb: async (params, user) => {
        const { newPassword } = params

        const privateKey = await bcrypt.genSalt(15)
        const encodePassword = await bcrypt.hash(newPassword + privateKey, privateKey)

        return {
            password: encodePassword,
            private_key: privateKey,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateFileForInsertToDb: (params) => {
        const { 
            filename,
            destination,
            mimetype,
            size } = params

        let path = destination.split('/')
        path = path[path.length - 1]
        
        return {
            id: uuid(),
            path: path,
            name: filename,
            type: mimetype,
            size: size,
            status: 1
        }
    },
    generateQRCodeSetForInsertToDb: (params, user, number) => {
        const { name, amount, description } = params

        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'QS'),
            number: number + 1,
            name: name, 
            amount: amount,
            status: 1,
            description: description,
            create_by: user.id,
            edit_by: user.id
        }
    },

    generateQRCodeItemForInsertToDb: (setId, user, number) => {
        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15),
            number: parseInt(number) + 1,
            usage_status: 1,
            set_id: setId[0],
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateCreatureForInsertToDb: (params, user, number) => {
        const { 
            qrcodeSetId,
            qrcodeItemId,
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            creatureImageId,
            weight,
            height,
            birthCertificateImageId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'CT'),
            number: number + 1,
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            weight: weight,
            height: height,
            profile_image_id: creatureImageId,
            birth_certificate_image_id: birthCertificateImageId,
            qrcode_set_id: qrcodeSetId,
            qrcode_item_id: qrcodeItemId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateCreatureForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            creatureImageId,
            weight,
            height,
            birthCertificateImageId } = params
            
         return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            weight: weight,
            height: height,
            profile_image_id: creatureImageId,
            birth_certificate_image_id: birthCertificateImageId,
            member_id: user.id,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateTreatmentHistoryForInsertToDb: (params, user, number) => {
        const { 
            date,
            time,
            description,
            department,
            fileCategory,
            creatureId,
            treatmentHistoryImageId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'TH'),
            number: number + 1,
            date: date,
            time: time,
            description: description,
            department: department,
            file_category: fileCategory,
            creature_id: creatureId,
            treatment_history_image_id: treatmentHistoryImageId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateTreatmentHistoryForUpdateToDb: (params, user) => {
        const { 
            date,
            time,
            description,
            department,
            fileCategory,
            treatmentHistoryImageId } = params

        const result = {
            date: date,
            time: time,
            description: description,
            department: department,
            file_category: fileCategory,
            treatment_history_image_id: treatmentHistoryImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        if (typeof treatmentHistoryImageId === 'undefined' || treatmentHistoryImageId === null ) delete treatmentHistory.treatment_history_image_id

        return result
    },
    generateMedicalForInsertToDb: (params, user, number) => {
        const { 
            hospitalName,
            hospitalTel,
            patientId,
            bloodType,
            doctorName,
            doctorContactNo,
            congenitalDisorder,
            creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'MD'),
            number: number + 1,
            hospital_name: hospitalName,
            hospital_tel: hospitalTel,
            patient_id: patientId,
            blood_type: bloodType,
            doctor_name: doctorName,
            doctor_contact_no: doctorContactNo,
            congenital_disorder: congenitalDisorder,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateMedicalForUpdateToDb: (params, user) => {
        const { 
            hospitalName,
            hospitalTel,
            patientId,
            bloodType,
            doctorName,
            doctorContactNo,
            congenitalDisorder,
            creatureId } = params

        const result = {
            hospital_name: hospitalName,
            hospital_tel: hospitalTel,
            patient_id: patientId,
            blood_type: bloodType,
            doctor_name: doctorName,
            doctor_contact_no: doctorContactNo,
            congenital_disorder: congenitalDisorder,
            creature_id: creatureId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateVaccineBookForInsertToDb: (params, user, number) => {
        const { 
            name: name,
            creatureId: creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'VB'),
            number: number + 1,
            name: name,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateVaccineBookForUpdateToDb: (params, user) => {
        const { name } = params

        const result = {
            name: name,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateVaccineForInsertToDb: (params, user, number) => {
        const { 
            hospitalName,
            date,
            vaccineBookId,
            vaccineImageId,
            creatureId } = params
        
        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'VC'),
            number: number + 1,
            hospital_name: hospitalName,
            date: date,
            vaccine_book_id: vaccineBookId,
            vaccine_image_id: vaccineImageId,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateVaccineForUpdateToDb: (params, user) => {
        const { 
            hospitalName,
            date,
            vaccineImageId } = params

        const result = {
            hospital_name: hospitalName,
            date: date,
            vaccine_image_id: vaccineImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateDrugForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId,
            drugImageId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'DG'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            drug_image_id: drugImageId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateDrugForUpdateToDb: (params, user) => {
        const { 
            name,
            description,
            drugImageId } = params

        const result = {
            name: name,
            description: description,
            drug_image_id: drugImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateDrugAllergyForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId,
            drugAllergyImageId } = params
            
         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'DA'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            drug_allergy_image_id: drugAllergyImageId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateDrugAllergyForUpdateToDb: (params, user) => {
        const { 
            name,
            description,
            drugAllergyImageId } = params

        const result = {
            name: name,
            description: description,
            drug_allergy_image_id: drugAllergyImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateFoodAllergyForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId,
            foodAllergyImageId } = params
            
         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'FA'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            food_allergy_image_id: foodAllergyImageId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateFoodAllergyForUpdateToDb: (params, user) => {
        const { 
            name,
            description,
            foodAllergyImageId } = params

        const result = {
            name: name,
            description: description,
            food_allergy_image_id: foodAllergyImageId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    }
}