if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' })
} else {
    require('dotenv').config({ path: 'development.env' })
}

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')
const con = require('./config/db.js')

const fs = require('fs')
const path = require('path')

const { createLogger, format, transports } = require('winston')
require('winston-daily-rotate-file')
const logDir = process.env.LOG_PATH

if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir)
}

const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%-results.log`,
    datePattern: 'YYYY-MM-DD'
})

const logger = createLogger({
    // change level if in dev environment versus production
    level: process.env.NODE_ENV === 'development' ? 'verbose' : 'info',
    format: format.combine(
        format.label({ label: path.basename(process.mainModule.filename) }),
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
        new transports.Console({
            level: 'info',
            format: format.combine(
                format.colorize(),
                format.printf(
                    info => `${info.timestamp} ${info.level}: ${info.message}`
                )
            )
        }),
        dailyRotateFileTransport
    ]
})

const app = express()
const port = 3000

// include router
const admin = require('./routes/admin')
const creature = require('./routes/creature')
const drug = require('./routes/drug')
const drugAllergy = require('./routes/drug-allergy')
const file = require('./routes/file')
const foodAllergy = require('./routes/food-allergy')
const medical = require('./routes/medical')
const member = require('./routes/member')
const qrcode = require('./routes/qrcode')
const template = require('./routes/template')
const treatmentHistory = require('./routes/treatment-history')
const vaccine = require('./routes/vaccine')

app.use(cors())
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(passport.session())

  
app.set('view engine', 'ejs')

app.use(function(req, res, next) {
    req.con = con
    req.logger = logger 
    next()
})

// routing
app.use('/api/admin', admin)
app.use('/api/creature', creature)
app.use('/api/drug', drug)
app.use('/api/drug-allergy', drugAllergy)
app.use('/api/file', file)
app.use('/api/food-allergy', foodAllergy)
app.use('/api/medical', medical)
app.use('/api/member', member)
app.use('/api/qrcode', qrcode)
app.use('/api/template', template)
app.use('/api/treatment-history', treatmentHistory)
app.use('/api/vaccine', vaccine)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
