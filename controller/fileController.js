const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, file, logger } = req
        console.log('file', file)

        try {

            return res.json(responseBuilder.error('Test'))
        } catch (error) {
            logger.error('fileController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}