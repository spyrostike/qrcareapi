const bcrypt = require('bcrypt')
const jwt = require("jwt-simple")
const Admin = require('../model/Admin')
const responseBuilder = require('../services/responseBuilder')


module.exports = {
    authen: async (req, res) => {
        const { con, body, logger } = req

        try {
            const admin = await Admin.authenticate(con, body)

            if (admin) {
                const password = body.password + admin.private_key

                const match = await bcrypt.compare(password, admin.password)

                if (match) {
                    const payload = {
                        id: admin.id,
                        iat: new Date().getTime(),
                        role: 'admin',
                        application: 'web-application'
                    }

                    return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))
                }

                return res.json(responseBuilder.error('Incorrect username or password.'))
            } else {
                return res.json(responseBuilder.error('Incorrect username or password.'))
            }
        } catch (error) {
            logger.error('adminController -> authen -> ' + error.message)
            return res.json(responseBuilder.error(error))
        }
        
    },
    getProfile: (req, res) => {
        return res.json(responseBuilder.success({ result: req.user }))
    }
}