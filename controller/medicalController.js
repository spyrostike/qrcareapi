const Medical = require('../model/Medical')
const responseBuilder = require('../services/responseBuilder')


module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const medical = await Medical.getMedicalByCreatureId(con, body)
            if (medical) return res.json(responseBuilder.error('Medical is already exist.'))
            await Medical.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('medicalController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }

    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Medical.update(con, body, user)
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('medicalController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }

    },
    getMedicalByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const medical = await Medical.getMedicalByCreatureId(con, params)
            return res.json(responseBuilder.success({ result: medical }))
        } catch (error) {
            logger.error('medicalController -> getMedicalByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}