const Drug = require('../model/Drug')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.drugImageId = addFile[0]
                }
            }

            await Drug.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('drugController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.drugImageId = addFile[0]
                }
            }

            await Drug.update(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('drugController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const drug = await Drug.getDrugById(con, params)

            if (drug) {
                const file = await File.getFileById(con, { id: drug.drug_image_id })
                if (file) {
                    drug.image = file.path + '/' + file.name
                }
                return res.json(responseBuilder.success({ result: drug }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('drugController -> getDrugById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await Drug.getDrugListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugController -> getDrugListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countDrugByCreatureId: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const count = await Drug.countDrugByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugController -> countDrugByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Drug.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('drugController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}