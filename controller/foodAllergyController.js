const FoodAllergy = require('../model/FoodAllergy')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.foodAllergyImageId = addFile[0]
                }
            }

            await FoodAllergy.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('foodAllergyController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {

            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.foodAllergyImageId = addFile[0]
                }
            }

            await FoodAllergy.update(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('foodAllergyController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFoodAllergyById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const foodAllergy = await FoodAllergy.getFoodAllergyById(con, params)

            if (foodAllergy) {
                const file = await File.getFileById(con, { id: foodAllergy.food_allergy_image_id })
                if (file) {
                    foodAllergy.image = file.path + '/' + file.name
                }
                return res.json(responseBuilder.success({ result: foodAllergy }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('foodAllergyController -> getFoodAllergyById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFoodAllergyListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await FoodAllergy.getFoodAllergyListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('foodAllergyController -> getFoodAllergyListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countFoodAllergyByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const count = await FoodAllergy.countFoodAllergyByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('foodAllergyController -> countFoodAllergyByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await FoodAllergy.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('foodAllergyController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}