const dayjs = require('dayjs')
const TreatmentHistory = require('../model/TreatmentHistory')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, file, user, logger } = req
        
        try {
            if (file) {
                const image = await File.add(con, file)
                if (image) body.treatmentHistoryImageId = image[0]
            }

            await TreatmentHistory.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('treatmentHistoryController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            if (file) {
                const image = await File.add(con, file)
                if (image) body.treatmentHistoryImageId = image[0]
            }

            await TreatmentHistory.update(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('treatmentHistoryController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getTreatmentHistoryById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const treatmentHistory = await TreatmentHistory.getTreatmentHistoryById(con, params)

            if (treatmentHistory) {
                treatmentHistory.date = dayjs(treatmentHistory.date, 'YYYY-MM-DD').format()
                const file = await File.getFileById(con, { id: treatmentHistory.treatment_history_image_id })
                if (file) {
                    treatmentHistory.image = file.path + '/' + file.name
                }
                return res.json(responseBuilder.success({ result: treatmentHistory }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('treatmentHistoryController -> getTreatmentHistoryById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getTreatmentHistoryListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await TreatmentHistory.getTreatmentHistoryListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('treatmentHistoryController -> getTreatmentHistoryListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await TreatmentHistory.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('treatmentHistoryController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}