const Creature = require('../model/Creature')
const Qrcode = require('../model/QRCode')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')
const dayjs = require('dayjs')

module.exports = {
    add: async (req, res) => {
        const { con, body, files, user, logger } = req

        try {
            const qrcodeItem =  await Qrcode.getQRCodeItemById(con, { id: body.qrcodeItemId })

            if (!qrcodeItem) return res.json(responseBuilder.error('QRCode not found.'))
            if (qrcodeItem.usage_status === 3 ) return res.json(responseBuilder.error('QRCode have been used.'))

            const creatureImage = await File.add(con, files[0])
            const birthCertificateImage = await File.add(con, files[1])

            if (creatureImage) body.creatureImageId = creatureImage[0]
            if (birthCertificateImage) body.birthCertificateImageId = birthCertificateImage[0]

            const creature = await Creature.add(con, body, user)
            
            if (creature) {
                creature[0].birth_date = dayjs(creature[0].birth_date).format('YYYY-MM-DD')
                creature[0].creatureImage = '/' + creatureImage.path + '/' + creatureImage.name
                creature[0].birthCertificateImage = '/' + birthCertificateImage.path + '/' + birthCertificateImage.name

                return res.json(responseBuilder.success({ result: creature[0] }))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('creatureController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, files, user, logger } = req
        let creatureImage = null
        let birthCertificateImage = null
        
        try {

            for (var i = 0;i < files.length; i ++) {
                const prefixImage = files[i].filename.substring(0, 2)
                const image = await File.add(con, files[i])
                if (image) {
                    if (prefixImage === 'CT') {
                        creatureImage = image
                        body.creatureImageId = image[0]
                     } else {
                        birthCertificateImage = image
                        body.birthCertificateImageId = image[0]
                     }
                }
                    
            }

            const creature = await Creature.update(con, body, user)
            
            if (creature) {
                creature[0].birth_date = dayjs(creature[0].birth_date).format('YYYY-MM-DD')
                if (creatureImage) creature[0].creatureImage = '/' + creatureImage.path + '/' + creatureImage.name
                if (birthCertificateImage) creature[0].birthCertificateImage = '/' + birthCertificateImage.path + '/' + birthCertificateImage.name

                return res.json(responseBuilder.success({ result: creature[0] }))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('creatureController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const creature = await Creature.getCreatureById(con, params)
            
            if (creature) {

                const profileImage = await File.getFileById(con, { id: creature.profile_image_id })

                if (profileImage) {
                    creature.profile_image = profileImage.path + '/' + profileImage.name
                }

                const birthCertificateImage = await File.getFileById(con, { id: creature.birth_certificate_image_id })

                if (birthCertificateImage) {
                    creature.birth_certificate_image = birthCertificateImage.path + '/' + birthCertificateImage.name
                }
                return res.json(responseBuilder.success({ result: creature }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureById -> ' + error.message)
            return res.json(responseBuilder.error(error.message))
        }
    },
    getCreatureByQRCodeId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const creature = await Creature.getCreatureByQRCodeId(con, params)

            if (creature) {
                return res.json(responseBuilder.success({ result: creature }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureByQRCodeId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureListByMemberId: async (req, res) => {
        const { con, params, user, logger } = req

        try {
            const result = await Creature.getCreatureListByMemberId(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: result }))
            } else {
                return res.json(responseBuilder.success({ result: [] }))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureListByMemberId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
   
        try {
            const result = await Creature.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }
        } catch (error) {
            logger.error('creatureController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}