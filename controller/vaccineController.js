const File = require('../model/File')
const dayjs = require('dayjs')
const Vaccine = require('../model/Vaccine')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// vaccine book ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addBook: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const id = await Vaccine.addBook(con, body, user)
            return res.json(responseBuilder.success({ result: { id: id[0] } }))
        } catch (error) {
            logger.error('vaccineController -> addBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateBook: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Vaccine.updateBook(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('vaccineController -> updateBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineBookListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const vaccineBook = await Vaccine.getVaccineBookListByCreatureId(con, params)
            return res.json(responseBuilder.success({ result: vaccineBook }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineBookListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineBookById: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const vaccine = await Vaccine.getVaccineBookById(con, params)
            if (vaccine) {
                return res.json(responseBuilder.success({ result: vaccine }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('vaccineController -> getVaccineBookById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteBook: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Vaccine.deleteBook(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('vaccineController -> deleteBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },

    ///////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// vaccine ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    add: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            const addFile = await File.add(con, file)

            if (addFile[0]) { 
                body.vaccineImageId = addFile[0]
            }

            await Vaccine.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('vaccineController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            const addFile = await File.add(con, file)

            if (addFile[0]) { 
                body.vaccineImageId = addFile[0]
            }

            await Vaccine.update(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('vaccineController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Vaccine.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('vaccineController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineListByBookId: async (req, res) => {
        const { con, params, logger } = req
    
        try {
            const vaccineList = await Vaccine.getVaccineListByBookId(con, params)
            return res.json(responseBuilder.success({ result: vaccineList }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineListByBookId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const vaccine = await Vaccine.getVaccineById(con, params)
            
            if (vaccine) {
                vaccine.date = dayjs(vaccine.date, 'YYYY-MM-DD').format()
                const file = await File.getFileById(con, { id: vaccine.vaccine_image_id })
                if (file) {
                    vaccine.image = file.path + '/' + file.name
                }
            }

            return res.json(responseBuilder.success({ result: vaccine }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}