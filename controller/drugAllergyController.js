const DrugAllergy = require('../model/DrugAllergy')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {
            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.drugAllergyImageId = addFile[0]
                }
            }

            await DrugAllergy.add(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been inserted.' }))
        } catch (error) {
            logger.error('drugAllergyController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {

            if (file) {
                const addFile = await File.add(con, file)

                if (addFile[0]) { 
                    body.drugAllergyImageId = addFile[0]
                }
            }

            await DrugAllergy.update(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('drugAllergyController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugAllergyById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const drugAllergy = await DrugAllergy.getDrugAllergyById(con, params)

            if (drugAllergy) {
                const file = await File.getFileById(con, { id: drugAllergy.drug_allergy_image_id })
                if (file) {
                    drugAllergy.image = file.path + '/' + file.name
                }
                return res.json(responseBuilder.success({ result: drugAllergy }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('drugAllergyController -> getDrugAllergyById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugAllergyListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await DrugAllergy.getDrugAllergyListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugAllergyController -> getDrugAllergyListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countDrugAllergyByCreatureId: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const count = await DrugAllergy.countDrugAllergyByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugAllergyController -> countDrugAllergyByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await DrugAllergy.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('drugAllergyController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}