const bcrypt = require('bcrypt')
const jwt = require("jwt-simple")
const Member = require('../model/Member')
const Qrcode = require('../model/QRCode')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    authen: async (req, res) => {
        const { con, body, logger } = req
        
        try {
            const member = await Member.checkUserNameExist(con, body)
            
            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Incorrect username or password.'))

                const password = body.password + member.private_key
                const match = await bcrypt.compare(password, member.password)

                if (match) {
                    const payload = {
                        id: member.id,
                        iat: new Date().getTime(),
                        role: 'member',
                        application: 'mobile-application'
                    }
                    
                    return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))

                }

                return res.json(responseBuilder.error('Incorrect username or password.'))
            } else {
                return res.json(responseBuilder.error('Incorrect username or password.'))
            }

        } catch (error) {
            logger.error('memberController -> authen -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong'))
        }
        
    },
    getProfile: (req, res) => {
        return res.json(responseBuilder.success({ result: req.user }))
    },
    checkUserNameExist: async (req, res) => {
        const { con, body, logger } = req

        try {
            const checkExistUserName = await Member.checkUserNameExist(con, body)

            if (checkExistUserName) {
                return res.json(responseBuilder.error('user is already exist'))
            } else {
                return res.json(responseBuilder.success('user does not exist'))
            }
        } catch (error) {
            logger.error('memberController -> checkUserNameExist -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong'))
        }
    },
    register: async (req, res) => {
        const { con, body, files, logger } = req

        try {
            const qrcodeItem = await Qrcode.getQRCodeItemById(con, { id: body.qrcodeItemId })

            if (!qrcodeItem) return res.json(responseBuilder.error('QRCode not found.'))
            if (qrcodeItem.usage_status !== 1) return res.json(responseBuilder.error('QRCode have been used.'))
            
            const profileImage = await File.add(con, files[0])
            if (profileImage) body.profileImageId = profileImage[0]

            const placeImage = await File.add(con, files[1])
            if (placeImage) body.placeImageId = placeImage[0]

            const checkExistUserName = await Member.checkUserNameExist(con, body)

            if (checkExistUserName) return res.json(responseBuilder.error('user is already exist.'))

            const member = await Member.register(con, body)

            if (member) {
                const payload = {
                    id: member[0],
                    iat: new Date().getTime(),
                    role: 'member',
                    application: 'mobile-application'
                }
                
                return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))
                
            }  else {
                return res.json(responseBuilder.error('Registing was failed.'))
            }
        } catch (error) {
            logger.error('memberController -> register -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, files, user, logger } = req

        try {

            for (var i = 0;i < files.length; i ++) {
                const prefixImage = files[i].filename.substring(0, 2)
                const image = await File.add(con, files[i])
                if (image) prefixImage === 'MB' ? body.profileImageId = image[0] : body.placeImageId = image[0]
            }
            
            await Member.update(con, body, user)

            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateProfile: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {

            if (file) {
                const image = await File.add(con, file)
                if (image) body.profileImageId = image[0]
            }

            await Member.updateProfile(con, body, user)

            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> updateProfile -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateLocation: async (req, res) => {
        const { con, body, file, user, logger } = req

        try {

            if (file) {
                const image = await File.add(con, file)
                if (image) body.placeImageId = image[0]
            }

            await Member.updateLocation(con, body, user)

            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> updateLocation -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    changePassword: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const member = await Member.getMemberByIdAllProps(con, user)
            console.log(member)

            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Account has been deleted'))

                const password = body.currentPassword + member.private_key
                const match = await bcrypt.compare(password, member.password)

                if (match) {
                    
                    await Member.changePassword(con, body, user)
                    
                    // return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))
                    return res.json(responseBuilder.success('Password has been changed'))
                } else {
                    return res.json(responseBuilder.error('Incorrect Current password.'))
                }
            } else {
                return res.json(responseBuilder.error('Member not found.'))
            }

        } catch (error) {
            logger.error('memberController -> changePassword -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getMemberList: async (req, res) => {
        const { con, query, logger } = req

        try {   
            const items = await Member.getMemberList(con, query)
            const records = await Member.getMemberCount(con, query)
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('memberController -> getMemberList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getMemberById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const member = await Member.getMemberById(con, params)
            if (member) {
                return res.json(responseBuilder.success({ result: member }))
            }
             
            return res.json(responseBuilder.error('Data not found.'))
        } catch (error) {
            logger.error('memberController -> getMemberById -> ' + error.message)
            return res.json(responseBuilder.error(error.message))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Member.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('memberController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}