const dayjs = require('dayjs')
const QRCodeLib = require('qrcode')
const _ = require('lodash')
const uuid = require('uuidv4').default
const QRCode = require('../model/QRCode')

module.exports = {
    qrcodeSetPattern: async (req, res) => {
        const { con, query, logger } = req

        try {
            let qrcodeSet = await QRCode.getQRCodeSetById(con, query)

            if (qrcodeSet) {
                const qrcodeItems = await QRCode.getQRCodeItemsByParentId(con, { parentId: qrcodeSet.id })

                items = await generateQRCode(qrcodeItems)

                    qrcodeSet = {
                        ...qrcodeSet,
                        items: items
                    }

                    const convertJSONDateToString = (value) => {
                        return dayjs(value).format('D/MM/YYYY')
                    }

                    const templateData = { 
                        parent: qrcodeSet, 
                        convertJSONDateToString: convertJSONDateToString
                    }

                    return res.render('../views/pattern/qrcode-set', templateData)
            } else {
                return res.status(404).send('404 Not found')
            }
        } catch (error) {
            logger.error('templateController -> qrcodeSetPattern -> ' + error.message)
            return res.status(404).send('404 Not found')
        }
    }
}

const generateQRCode = (items) => {
    return new Promise(async (resolve, reject) => { 
        for (var i = 0; i < items.length ; i++) {
            const qrcode = await generateQR(items[i].id)
            items[i].qrcode = qrcode
        }
        
        resolve(_.chunk( _.chunk(items, 3), 7))
    })
}

const generateQR = async text => {
    try {
      return await QRCodeLib.toDataURL(text)
    } catch (err) {
      console.error(err)
    }
}