exports.up = function(knex) {
    return knex.schema.createTable('medical', function(table) {
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.string('hospital_name', 60).notNullable()
        table.string('hospital_tel', 15)
        table.string('patient_id', 30).notNullable()
        table.integer('blood_type').notNullable()
        table.string('doctor_name', 100).notNullable()
        table.string('doctor_contact_no', 10)
        table.string('congenital_disorder', 100)
        table.uuid('creature_id').notNullable()
        table.uuid('member_id').notNullable()
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('creature_id').references('id').inTable('creature')
        table.foreign('blood_type').references('id').inTable('blood_type')
        table.foreign('member_id').references('id').inTable('member')
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
