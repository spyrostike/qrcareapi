const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('file_category', function(table) {
        // table.increments('id')
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
    }).then(function () {
        return knex('file_category').insert(
            [
                { id: 1, name_eng: '-Check up', name_th: '-Check up' },
                { id: 2, name_eng: '-X ray', name_th: '-X ray' },
                { id: 3, name_eng: '-MRI / CT scan', name_th: '-MRI / CT scan' },
                { id: 4, name_eng: '-Test  result', name_th: '-Test  result' },
                { id: 5, name_eng: '-X ray', name_th: '-Other' }
            ]
        )
        
    })
}

exports.down = function(knex) {}
