exports.up = function(knex) {
    return knex.schema.createTable('treatment_history', function(table) {
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.string('description', 1200)
        table.integer('department').unsigned()
        table.integer('file_category').unsigned()
        table.uuid('creature_id').notNullable()
        table.date('date', { precision: 6, useTz: true }).notNullable()
        table.time('time', { precision: 6 }).notNullable()
        table.uuid('member_id').notNullable()
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('department').references('id').inTable('department')
        table.foreign('file_category').references('id').inTable('file_category')
        table.foreign('creature_id').references('id').inTable('creature')
        table.foreign('member_id').references('id').inTable('member')
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
