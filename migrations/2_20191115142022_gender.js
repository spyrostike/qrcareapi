const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('gender', function(table) {
        // table.increments('id')
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
            
    }).then(function () {
        return knex('gender').insert(
            [
                { id: 1, name_eng: 'male', name_th: 'ชาย' },
                {  id: 2, name_eng: 'female', name_th: 'หญิง' }
            ]
        )
        
    })
}

exports.down = function(knex) {}
