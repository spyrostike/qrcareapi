const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('usage_status', function(table) {
        // table.increments('id')
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
            
    }).then(function () {
        return knex('usage_status').insert(
            [
                { id: 1, name_eng: 'not in use', name_th: 'ยังไม่ถูกใช้งาน' },
                { id: 2, name_eng: 'registered', name_th: 'ถูกใช้ในการสมัครสมาชิก' },
                { id: 3, name_eng: 'created creature', name_th: 'ถูกใช้ในการสร้างข้อมูล' }
            ]
        )
        
    })
}

exports.down = function(knex) {}
