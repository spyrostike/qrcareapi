const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('department', function(table) {
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

    }).then(function () {
        return knex('department').insert(
            [
                { id: 1, name_eng: 'Medicine', name_th: 'Medicine' },
                { id: 2, name_eng: 'Ear nose throat', name_th: 'Ear nose throat' },
                { id: 3, name_eng: 'Teeth', name_th: 'Teeth' },
                { id: 4, name_eng: 'Endocrine system', name_th: 'Endocrine system' },
                { id: 5, name_eng: 'Heart', name_th: 'Heart' },
                { id: 6, name_eng: 'Lung', name_th: 'Endocrine system' },
                { id: 7, name_eng: 'Liver', name_th: 'Liver' },
                { id: 8, name_eng: 'Kidney', name_th: 'Kidney' },
                { id: 9, name_eng: 'Bone & Joint', name_th: 'Bone & Joint' }
            ]
        )
        
    })
}

exports.down = function(knex) {}
