exports.up = function(knex) {
    return knex.schema.createTable('file', function(table) {
        table.uuid('id').primary()
        table.string('path', 20)
        table.string('name', 30)
        table.string('type', 20)
        table.string('sub_type', 20)
        table.integer('size')
        table.uuid('ref_id')
        table.string('tag', 50).notNullable()
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
