const passport = require('passport')
const ExtractJwt = require('passport-jwt').ExtractJwt
const JwtStrategy = require('passport-jwt').Strategy

const con = require('../config/db.js')
const Admin = require('../model/Admin')
const Member = require('../model/Member')

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET_KEY
}

const jwtAuth = new JwtStrategy(jwtOptions, async (payload, done) => {
    if (payload.role === 'admin') {
        try {
            const admin = await Admin.getAdminById(con, payload)

            if (admin) return done(null, admin) 
            else return done(null, false) 
        } catch (error) {
            return done(null, false) 
        }
    } else if (payload.role === 'member') {
        try {
            const member = await Member.getMemberById(con, payload)

            if (member) return done(null, member) 
            else return done(null, false) 
        } catch (error) {
            return done(null, false) 
        }
    }
})

passport.use(jwtAuth)

const requireJWTAuth = passport.authenticate('jwt', { session: false })

module.exports = {
    requireJWTAuth: requireJWTAuth
}