module.exports = {
    success: (data) => {
        return {
            status: '00',
            errorMessage: null,
            data: data
        }
    },
    error: (errorMessage, data) => {
        return {
            status: '10',
            errorMessage: errorMessage,
            data: data
        }
    }
}