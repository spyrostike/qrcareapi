const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const fileController = require('../controller/fileController')
const jwtHelper = require('../services/jwtHelper')
const dirHelper = require('../utils/dirHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            // if (file.mimetype === 'image/jpeg')
            let prefix = ''
            if (file.originalname === 'member-profile.js') prefix = 'MB'

            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/add', upload.single('filedata'), fileController.add)

module.exports = router