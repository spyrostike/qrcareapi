const express = require('express')
const router = express.Router()
const adminController = require('../controller/adminController')
const jwtHelper = require('../services/jwtHelper')

router.post('/authen', adminController.authen)
router.get('/profile', jwtHelper.requireJWTAuth, adminController.getProfile)

module.exports = router