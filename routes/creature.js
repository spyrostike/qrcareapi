const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const creatureController = require('../controller/creatureController')
const jwtHelper = require('../services/jwtHelper')
const dirHelper = require('../utils/dirHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            const prefix = file.originalname === 'creatureImage.jpg' ? 'CT' : 'BC'
 
            
            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/add', jwtHelper.requireJWTAuth, upload.array('images', 2), creatureController.add)
router.put('/update', jwtHelper.requireJWTAuth, upload.array('images', 2), creatureController.update)
router.get('/list', jwtHelper.requireJWTAuth, creatureController.getCreatureListByMemberId)
router.get('/creature-by-qrcode-id/:qrcode_id', creatureController.getCreatureByQRCodeId)
router.get('/:id', creatureController.getCreatureById)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, creatureController.delete)

module.exports = router