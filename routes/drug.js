const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const drugController = require('../controller/drugController')
const jwtHelper = require('../services/jwtHelper')
const dirHelper = require('../utils/dirHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            const prefix = 'DG'
 
            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/add', jwtHelper.requireJWTAuth, upload.single('image'), drugController.add)
router.put('/update', jwtHelper.requireJWTAuth, upload.single('image', 1), drugController.update)
router.get('/list/:creature_id', drugController.getDrugListByCreatureId)
router.get('/:id', drugController.getDrugById)
router.get('/count/:creature_id', drugController.countDrugByCreatureId)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, drugController.delete)

module.exports = router