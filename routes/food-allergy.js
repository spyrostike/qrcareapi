const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const foodAllergyController = require('../controller/foodAllergyController')
const jwtHelper = require('../services/jwtHelper')
const dirHelper = require('../utils/dirHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            const prefix = 'FA'
 
            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/add', jwtHelper.requireJWTAuth, upload.single('image'), foodAllergyController.add)
router.put('/update', jwtHelper.requireJWTAuth, upload.single('image', 1), foodAllergyController.update)
router.get('/list/:creature_id', foodAllergyController.getFoodAllergyListByCreatureId)
router.get('/:id', foodAllergyController.getFoodAllergyById)
router.get('/count/:creature_id', foodAllergyController.countFoodAllergyByCreatureId)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, foodAllergyController.delete)

module.exports = router