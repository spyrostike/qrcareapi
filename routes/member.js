const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const memberController = require('../controller/memberController')
const jwtHelper = require('../services/jwtHelper')
const dirHelper = require('../utils/dirHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            const prefix = file.originalname === 'profileImage.jpg' ? 'MB' : 'PC'
            // if (!file.mimetype.match(/\/(jpg)$/)) {
            //     mimetype = '.jpg'
            // }
            
            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/authen', memberController.authen)
router.get('/profile', jwtHelper.requireJWTAuth, memberController.getProfile)
router.get('/get-member-list', jwtHelper.requireJWTAuth, memberController.getMemberList)
router.get('/:id', memberController.getMemberById)
router.post('/check-username-exist', memberController.checkUserNameExist)
router.post('/register', upload.array('images', 2), memberController.register)
router.put('/update', jwtHelper.requireJWTAuth, upload.array('images', 2), memberController.update)
router.put('/update-profile', jwtHelper.requireJWTAuth, upload.single('image'), memberController.updateProfile)
router.put('/update-location', jwtHelper.requireJWTAuth, upload.single('image'), memberController.updateLocation)
router.put('/change-password', jwtHelper.requireJWTAuth, memberController.changePassword)
router.delete('/:id', jwtHelper.requireJWTAuth, memberController.delete)

module.exports = router