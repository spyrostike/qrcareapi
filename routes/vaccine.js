const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const dirHelper = require('../utils/dirHelper')
const jwtHelper = require('../services/jwtHelper')
const vaccineController = require('../controller/vaccineController')

const router = express.Router()
const multer = require('multer')


const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            const prefix = 'VC'
 
            
            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// vaccine book ////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

router.post('/book/add', jwtHelper.requireJWTAuth, vaccineController.addBook)
router.put('/book/update', jwtHelper.requireJWTAuth, vaccineController.updateBook)
router.get('/book/list/:creature_id', vaccineController.getVaccineBookListByCreatureId)
router.get('/book/:id', vaccineController.getVaccineBookById)
router.delete('/book/:id', jwtHelper.requireJWTAuth, vaccineController.deleteBook)

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// vaccine ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

router.post('/add', jwtHelper.requireJWTAuth, upload.single('image', 1), vaccineController.add)
router.put('/update', jwtHelper.requireJWTAuth, upload.single('image', 1), vaccineController.update)
router.get('/list/:vaccine_book_id', vaccineController.getVaccineListByBookId)
router.get('/:id', vaccineController.getVaccineById)
router.delete('/:id', jwtHelper.requireJWTAuth, vaccineController.delete)

module.exports = router