const express = require('express')
const router = express.Router()
const qrcodeController = require('../controller/qrcodeController')

const jwtHelper = require('../services/jwtHelper')

router.post('/generate', jwtHelper.requireJWTAuth, qrcodeController.generate)
router.get('/get-qrcode-set-list', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeSetList)
router.get('/get-qrcode-set-by-id/:id', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeSetById)
router.get('/generate-pdf/:id', qrcodeController.genetatePDF)
router.put('/update-qrcode-set', jwtHelper.requireJWTAuth, qrcodeController.updateQRCodeSet)
router.get('/get-qrcode-item-by-id/:id', qrcodeController.getQRCodeItemById)

module.exports = router