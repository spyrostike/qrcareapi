const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('treatment_history').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const treatmentHistory = ObjectUtil.generateTreatmentHistoryForInsertToDb(params, user, number)
   
        return await con.transaction((trx)  => {
            con('treatment_history')
            .transacting(trx)
            .returning('id')
            .insert(treatmentHistory)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const treatmentHistory = ObjectUtil.generateTreatmentHistoryForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('treatment_history')
            .update(treatmentHistory)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getTreatmentHistoryById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con({ th: 'treatment_history' })
            .select({
                id: 'th.id',
                code: 'th.code',
                number: 'th.number',
                description: 'th.description',
                department: 'th.department',
                file_category: 'th.file_category',
                creature_id: 'th.creature_id',
                date: 'th.date',
                time: 'th.time',
                treatment_history_image_id: 'th.treatment_history_image_id',
                status: 'th.status',
                create_date: 'th.create_date',
                create_by: 'th.create_by',
                edit_date: 'th.edit_date',
                edit_by: 'th.edit_by',
                department_name: 'dm.name_eng',
                file_category_name: 'fc.name_eng'
            })
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .where('th.status', iStatus)
            .andWhere('th.id', id)
            .orderBy('th.number', 'desc')
            .first()
    },
    getTreatmentHistoryListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ th: 'treatment_history' })
            .select({
                id: 'th.id',
                code: 'th.code',
                number: 'th.number',
                description: 'th.description',
                department: 'th.department',
                department_name: 'dm.name_eng',
                file_category: 'th.file_category',
                file_category_name: 'fc.name_eng',
                date: 'th.date',
                time: 'th.time',
                treatment_history_image_id: 'th.treatment_history_image_id',
                treatment_history_image: con.raw('concat( f.path, \'/\', f.name )'),
                creature_id: 'th.creature_id',
                member_id: 'th.member_id',
                status: 'th.status',
                create_date: 'th.create_date',
                create_by: 'th.create_by',
                edit_date: 'th.edit_date',
                edit_by: 'th.edit_by',
            })
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .leftJoin('file as f', 'th.treatment_history_image_id', 'f.id')
            .where('th.status', iStatus)
            .andWhere('th.creature_id', creature_id)
            .orderBy('th.number', 'asc')
    },
    delete: async (con, params, user) => {
        return await con('treatment_history')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}