const ObjectUtil = require('../utils/ObjectUtil')
const uuid = require('uuidv4').default
const sqlHelper = require('../utils/sqlHelper')
const dayjs = require('dayjs')

module.exports = {
    add: async (con, params, user) => {
        const { qrcodeItemId } = params

        let rowNo = await con('drug').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const creature = ObjectUtil.generateCreatureForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('qrcode_item')
            .transacting(trx)
            .returning('id')
            .update({  
                usage_status: 3,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            }).where('id', qrcodeItemId)
            .then(id => {
                return con('creature')
                .insert(creature)
                .transacting(trx)
                .returning('*')
            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const creature = ObjectUtil.generateCreatureForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('creature')
            .update(creature)
            .returning('*')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getCreatureById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                profile_image_id: 'ct.profile_image_id',
                birth_certificate_image_id: 'ct.birth_certificate_image_id',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .where('ct.status', iStatus)
            .andWhere('ct.id', id)
            .first()
    },
    getCreatureByQRCodeId: async (con, params) => {
        const { qrcode_id, status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                profile_image_id: 'ct.profile_image_id',
                birth_certificate_image_id: 'ct.birth_certificate_image_id',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .where('ct.status', iStatus)
            .andWhere('qrcode_item_id', qrcode_id)
            .first()
    },
    getCreatureListByMemberId: async (con, params, user) => {
        const { status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                profile_image_id: 'ct.profile_image_id',
                birth_certificate_image_id: 'ct.birth_certificate_image_id',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng',
                profile_image: con.raw('concat( f.path, \'/\', f.name )'),
                birth_certificate_image: con.raw('concat( fl.path, \'/\', fl.name )')
            })
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .leftJoin('file as f', 'ct.profile_image_id', 'f.id')
            .leftJoin('file as fl', 'ct.birth_certificate_image_id', 'fl.id')
            .where('ct.status', iStatus)
            .andWhere('ct.member_id', user.id)
            .orderBy('number', 'asc')
        
    },
    delete: async (con, params, user, callback) => {
        return await con('creature')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
            .asCallback(callback)
    }
}