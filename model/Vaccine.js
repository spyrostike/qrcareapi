const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// vaccine book ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addBook: async (con, params, user) => {
        let rowNo = await con('vaccine_book').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const vaccineBook = ObjectUtil.generateVaccineBookForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('vaccine_book')
            .transacting(trx)
            .returning('id')
            .insert(vaccineBook)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateBook: async  (con, params, user) => {
        const { id } = params

        const vaccineBook = ObjectUtil.generateVaccineBookForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('vaccine_book')
            .update(vaccineBook)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    deleteBook: async (con, params, user) => {
        return await con('vaccine_book')
            .update({
                status: 0,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    getVaccineBookListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ vb: 'vaccine_book' })
            .select({
                id: 'vb.id',
                code: 'vb.code',
                number: 'vb.number',
                name: 'vb.name',
                vaccine_type_name: 'vt.name_eng',
                creature_id: 'vb.creature_id',
                member_id: 'vb.member_id',
                status: 'vb.status',
                create_date: 'vb.create_date',
                create_by: 'vb.create_by',
                edit_date: 'vb.edit_date',
                edit_by: 'vb.edit_by'
            })
            .innerJoin('vaccine_type as vt', 'vb.name', 'vt.id')
            .where('vb.status', iStatus)
            .andWhere('vb.creature_id', creature_id)
            .orderBy('vb.number', 'asc')
    },
    getVaccineBookById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con({ vb: 'vaccine_book' })
            .select({
                id: 'vb.id',
                code: 'vb.code',
                number: 'vb.number',
                name: 'vb.name',
                creature_id: 'vb.creature_id',
                member_id: 'vb.member_id',
                status: 'vb.status',
                create_date: 'vb.create_date',
                create_by: 'vb.create_by',
                edit_date: 'vb.edit_date',
                edit_by: 'vb.edit_by',
                vaccine_type_name: 'vt.name_eng'
            })
            .innerJoin('vaccine_type as vt', 'vb.name', 'vt.id')
            .where('vb.status', iStatus)
            .andWhere('vb.id', id)
            .orderBy('vb.number', 'desc')
            .first()

        return await con('vaccine_book')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },

    ///////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// vaccine ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    add: async (con, params, user) => {
        let rowNo = await con('vaccine').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const vaccine = ObjectUtil.generateVaccineForInsertToDb(params, user, number)
       
        return await con.transaction((trx)  => {
            con('vaccine')
            .transacting(trx)
            .returning('id')
            .insert(vaccine)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params
        const vaccine = ObjectUtil.generateVaccineForUpdateToDb(params, user)
        
        return await con.transaction((trx)  => {
            con('vaccine')
            .transacting(trx)
            .update(vaccine)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getVaccineListByBookId: async (con, params) => {
        const { vaccine_book_id, status } = params 
        const iStatus = status || 1
        
        return await con('vaccine')
            .select('*')
            .where('status', iStatus)
            .andWhere('vaccine_book_id', vaccine_book_id)
            .orderBy('number', 'asc')
    },
    getVaccineById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1

        return await con('vaccine')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    delete: async (con, params, user) => {
        return await con('vaccine')
            .update({
                status: 0,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}