const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('medical').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const medical = ObjectUtil.generateMedicalForInsertToDb(params, user, number)
   
        return await con.transaction((trx)  => {
            con('medical')
            .transacting(trx)
            .returning('id')
            .insert(medical)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const medical = ObjectUtil.generateMedicalForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('medical')
            .update(medical)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getMedicalByCreatureId: async (con, params) => {
        const { creatureId, status } = params
        const iStatus = status || 1
        
        return await con({ md: 'medical' })
            .select({
                id: 'md.id',
                code: 'md.code',
                number: 'md.number',
                hospital_name: 'md.hospital_name',
                hospital_tel: 'md.hospital_tel',
                patient_id: 'md.patient_id',
                blood_type: 'md.blood_type',
                doctor_name: 'md.doctor_name',
                doctor_contact_no: 'md.doctor_contact_no',
                congenital_disorder: 'md.congenital_disorder',
                creature_id: 'md.creature_id',
                member_id: 'md.member_id',
                status: 'md.status',
                create_date: 'md.create_date',
                create_by: 'md.create_by',
                edit_date: 'md.edit_date',
                edit_by: 'md.edit_by',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('blood_type as bt', 'md.blood_type', 'bt.id')
            .where('md.status', iStatus)
            .andWhere('md.creature_id', creatureId)
            .orderBy('md.number', 'desc')
            .first()
    }
}
