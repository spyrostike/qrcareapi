
module.exports = {
    authenticate: async (con, params) => {
        return await con.select('*').from('admin').where('username', params.username).andWhere('status', 1).first()
    },
    getAdminById: async (con, params) => {
        return await con.select('*').from('admin').where('id', params.id).andWhere('status', 1).first()
    }
}