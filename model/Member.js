const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    authenticate: async (con, params) => {
        return await con.select('*').from('member').where('id', params.id).andWhere('status', 1).first()
    },
    checkUserNameExist: async (con, params) => {
        return await con.select('*').from('member').where('username', params.username).first()
    },
    register: async (con, params) => {
        const { qrcodeItemId } = params
        
        let rowNo = await con('member').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const member = await ObjectUtil.generateMemberForInsertToDb(params, number)

        return await con.transaction((trx)  => {
            con('member')
            .transacting(trx)
            .returning('id')
            .insert(member)
            .then(memberId => {
                return con('qrcode_item')
                .update({  
                    usage_status: 2,
                    edit_by: memberId[0],
                    edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
                }).where('id', qrcodeItemId)
                .transacting(trx)
                .returning('edit_by')
            })
            .then(trx.commit)
            .catch(trx.rollback)

        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const member = ObjectUtil.generateMemberForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateProfile: async (con, params, user) => {

        const member = ObjectUtil.generateMemberProfileForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateLocation: async (con, params, user) => {

        const member = ObjectUtil.generateMemberLocationForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    changePassword: async (con, params, user) => {
        const member = await ObjectUtil.generateMemberChangePasswordForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getMemberByQRCodeItemId: async (con, params) => {
        const { qrcodeItemId, status } = params

        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'first_name',
                last_name: 'last_name',
                gender: 'gender',
                blood_type: 'blood_type',
                birth_date: 'birth_date',
                relationship: 'relationship',
                identification_no: 'identification_no',
                contact_no1: 'contact_no1',
                contact_no2: 'contact_no2',
                address1: 'address1',
                address2: 'address2',
                address3: 'address3',
                profile_image_id: 'profile_image_id',
                place_image_id: 'place_image_id',
                place_latitude: 'place_latitude',
                place_longitude: 'place_longitude',
                qrcode_item_id: 'qrcode_item_id',
                qrcode_item_id: 'qrcode_item_id',
                status: 'status',
                create_date: 'create_date',
                create_by: 'create_by',
                edit_date: 'edit_date',
                edit_by: 'edit_by'

            })
            .where('mb.status', iStatus)
            .andWhere('mb.qrcode_item_id', qrcodeItemId)
            .first()
    },
    getMemberList: async (con, params) => {
        const { date, limit, offset, orderByKey, orderByValue, status, textSearch } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iTextSearch = textSearch || ''
        // const iDate = date ||  dayjs().format('YYYY-MM-D')
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || null

        const whereFirst = iStatus !== null ? 'qs.status = :status and ' : ''

        return await con({ qs: 'member'})
            .select(
                {
                    id: 'qs.id',
                    code: 'qs.code',
                    number: 'qs.number',
                    first_name: 'qs.first_name',
                    last_name: 'qs.last_name',
                    gender: 'qs.gender',
                    blood_type: 'qs.blood_type',
                    birth_date: 'qs.birth_date',
                    relationship: 'qs.relationship',
                    identification_no: 'qs.identification_no',
                    contact_no1: 'qs.contact_no1',
                    contact_no2: 'qs.contact_no2',
                    address1: 'qs.address1',
                    address2: 'qs.address2',
                    address3: 'qs.address3',
                    status: 'qs.status',
                    create_date: 'qs.create_date',
                    create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.create_by )')
                }
            )
            .whereRaw(
                whereFirst + '( qs.code like :value or qs.first_name like :value or qs.last_name like :value or qs.contact_no1 like :value )', 
                {
                    status: iStatus,
                    value: iTextSearch + '%'
                }
            )
            // .where('qs.status', iStatus)
            // .andWhere('qs.code', 'like', `${iTextSearch}%`)
            // .orWhere('qs.first_name', 'like', `${iTextSearch}%`)
            // .orWhere('qs.last_name', 'like', `${iTextSearch}%`)
            // .orWhere('qs.contact_no1', 'like', `${iTextSearch}%`)
            // .orWhere(con.raw(`cast (qs.create_date as date) = ?`, iTextSearch))
            .orderBy('qs.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getMemberCount: async (con, params) => {
        const { date, textSearch, status } = params

        // const iDate = date || dayjs().format('YYYY-MM-D')
        const iTextSearch = textSearch || ''
        const iStatus = status || null

        const whereFirst = iStatus !== null ? 'qs.status = :status and ' : ''

        return await con('member')
            .count('*')
            .whereRaw(
                whereFirst + '( code like :value or first_name like :value or last_name like :value or contact_no1 like :value )', 
                {
                    status: iStatus,
                    value: iTextSearch + '%'
                }
            )
            // .where('status', iStatus)
            // .andWhere('code', 'like', `${iTextSearch}%`)
            // .orWhere('first_name', 'like', `%${iTextSearch}%`)
            // .orWhere('last_name', 'like', `${iTextSearch}%`)
            // .orWhere('contact_no1', 'like', `${iTextSearch}%`)
            // .orWhere(con.raw(`cast (create_date as date) = ?`, iTextSearch))
    },
    getMemberByIdAllProps: async (con, params) => {
        const { id, status } = params
        
        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                profile_image_id: 'mb.profile_image_id',
                profile_image: con.raw('( select concat( f.path, \'/\', f.name ) from file f where  f.id = mb.profile_image_id )'),
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'mb.first_name',
                last_name: 'mb.last_name',
                gender: 'mb.gender',
                blood_type: 'mb.blood_type',
                birth_date: 'mb.birth_date',
                relationship: 'mb.relationship',
                identification_no: 'mb.identification_no',
                contact_no1: 'mb.contact_no1',
                contact_no2: 'mb.contact_no2',
                address1: 'mb.address1',
                address2: 'mb.address2',
                address3: 'mb.address3',
                username: 'mb.username',
                password: 'mb.password',
                private_key: 'mb.private_key',
                place_image_id: 'mb.place_image_id',
                place_image: con.raw('( select concat( f.path, \'/\', f.name ) from file f where  f.id = mb.place_image_id )'),
                place_latitude: 'mb.place_latitude',
                place_longitude: 'mb.place_longitude',
                status: 'mb.status',
                create_date: 'mb.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )'),
                create_date: 'mb.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.edit_by )'),
                edit_date: 'mb.edit_date',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'mb.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'mb.blood_type', 'bt.id')
            .where('mb.status', iStatus)
            .andWhere('mb.id', id)
            .first()
    },
    getMemberById: async (con, params) => {
        const { id, status } = params
        
        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                profile_image_id: 'mb.profile_image_id',
                profile_image: con.raw('( select concat( f.path, \'/\', f.name ) from file f where  f.id = mb.profile_image_id )'),
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'mb.first_name',
                last_name: 'mb.last_name',
                gender: 'mb.gender',
                blood_type: 'mb.blood_type',
                birth_date: 'mb.birth_date',
                relationship: 'mb.relationship',
                identification_no: 'mb.identification_no',
                contact_no1: 'mb.contact_no1',
                contact_no2: 'mb.contact_no2',
                address1: 'mb.address1',
                address2: 'mb.address2',
                address3: 'mb.address3',
                username: 'mb.username',
                place_image_id: 'mb.place_image_id',
                place_image: con.raw('( select concat( f.path, \'/\', f.name ) from file f where f.id = mb.place_image_id )'),
                place_latitude: 'mb.place_latitude',
                place_longitude: 'mb.place_longitude',
                status: 'mb.status',
                create_date: 'mb.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )'),
                create_date: 'mb.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.edit_by )'),
                edit_date: 'mb.edit_date',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'mb.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'mb.blood_type', 'bt.id')
            .where('mb.status', iStatus)
            .andWhere('mb.id', id)
            .first()
    },
    delete: async (con, params, user) => {
        return await con('member')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}