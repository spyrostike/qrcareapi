const uuid = require('uuidv4').default
const dayjs = require('dayjs')
const sqlHelper = require('../utils/sqlHelper')
const ObjectUtil = require('../utils/ObjectUtil')


module.exports = {
    generate: async (con, params, user) => {
        const { amount } = params
        
        let rowNo = await con('qrcode_set').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const qrcodeSet = ObjectUtil.generateQRCodeSetForInsertToDb(params, user, number)

        return await con.transaction((trx) => {
            con('qrcode_set').transacting(trx).returning('id').insert(qrcodeSet, 'id')
            .then( async (setId) => {
                let qrcodeItems = []
                let rowNo = await con('qrcode_item').select('number').orderBy('number', 'desc').first()
                let number = 0
                if (rowNo) number = rowNo.number

                for (var i = 0; i < amount; i++ ) {
                    const qrcodeItem = ObjectUtil.generateQRCodeItemForInsertToDb(setId, user, number + i)
                    qrcodeItems.push(qrcodeItem)
                }

                return con('qrcode_item').insert(qrcodeItems).transacting(trx)

            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getQRCodeSetList: async (con, params) => {
        const { date, limit, offset, orderByKey, orderByValue, status, textSearch } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iTextSearch = textSearch || ''
        const iDate = date ||  dayjs().format('YYYY-MM-D')
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        return await con({ qs: 'qrcode_set'})
            .select(
                {
                    id: 'qs.id',
                    code: 'qs.code',
                    number: 'qs.number',
                    name: 'qs.name',
                    amount: 'qs.amount',
                    status: 'qs.status',
                    code: 'qs.code',
                    create_date: 'qs.create_date',
                    create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.create_by )'),
                    code: 'qs.code',

                }
            )
            .where('qs.status', iStatus)
            .andWhere('qs.name', 'like', `${iTextSearch}%`)
            .andWhere(con.raw(`cast (qs.create_date as date) = ?`, iDate))
            .orderBy('qs.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getQRCodeSetCount: async (con, params) => {
        const { date, textSearch, status } = params

        const iDate = date || dayjs().format('YYYY-MM-D')
        const iTextSearch = textSearch || ''
        const iStatus = status || 1

        return await con('qrcode_set')
            .count('*')
            .where('status', iStatus)
            .andWhere('name', 'like', `%${iTextSearch}%`)
            .andWhere(con.raw(`cast (create_date as date) = ?`, iDate))
    },
    getQRCodeSetById: async (con, params) => {
        const { id, status } = params
        
        const iStatus = status || 1

        return await con({ qs: 'qrcode_set' })
            .select({
                id: 'qs.id',
                code: 'qs.code',
                number: 'qs.number',
                name: 'qs.name',
                description: 'qs.description', 
                amount: 'qs.amount',
                status: 'qs.status',
                create_date: 'qs.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.create_by )'),
                create_date: 'qs.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.edit_by )'),
                edit_date: 'qs.edit_date'
            })
            .where('qs.status', iStatus)
            .andWhere('qs.id', id)
            .first()
    },
    getQRCodeItemsByParentId: async (con, params) => {
        const { parentId } = params

        return await con({ qi: 'qrcode_item' })
            .select({
                id: 'qi.id',
                code: 'qi.code',
                number: 'qi.number',
                status: 'qi.status',
                usage_status: 'qi.usage_status',
                code: 'qi.code',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.create_by )'),
                create_date: 'qi.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.edit_by )'),
                edit_date: 'qi.create_date',
                code: 'qi.code'
            })
            .andWhere('set_id', parentId)
            .orderBy('number', 'asc')
    },
    updateQRCodeSet: async (con, params, user) => {
        const { id, name, description } = params 

        return await con.transaction((trx)  => {
            con('qrcode_set')
            .where('status', 1)
            .andWhere('id', id)
            .update({
                name: name,
                description: description,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateQRCodeItem: async (con, params, user) => {
        const { id, usageStatus, status } = params 

        return await con.transaction((trx)  => {
            con('qrcode_item')
            .where('status', 1)
            .andWhere('id', id)
            .update({
                usage_status: usageStatus,
                status: status,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getQRCodeItemById: async (con, params, callback) => {
        const { id, status } = params 

        const iStatus = status || 1

        return await con('qrcode_item')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
            .asCallback(callback)
    }
}