const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('food_allergy').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const foodAllergy = ObjectUtil.generateFoodAllergyForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('food_allergy')
            .transacting(trx)
            .returning('id')
            .insert(foodAllergy)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const foodAllergy = ObjectUtil.generateFoodAllergyForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('food_allergy')
            .update(foodAllergy)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getFoodAllergyById: async (con, params, callback) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('food_allergy')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getFoodAllergyListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ fa: 'food_allergy' })
            .select({
                id: 'fa.id',
                code: 'fa.code',
                number: 'fa.number',
                name: 'fa.name',
                description: 'fa.description',
                food_allergy_image_id: 'fa.food_allergy_image_id',
                food_allergy_image: con.raw('concat( f.path, \'/\', f.name )'),
                creature_id: 'fa.creature_id',
                member_id: 'fa.member_id',
                status: 'fa.status',
                create_date: 'fa.create_date',
                create_by: 'fa.create_by',
                edit_date: 'fa.edit_date',
                edit_by: 'fa.edit_by'
            })
            .leftJoin('file as f', 'fa.food_allergy_image_id', 'f.id')
            .where('fa.status', iStatus)
            .andWhere('fa.creature_id', creature_id)
            .orderBy('fa.number', 'asc')
    },
    countFoodAllergyByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('food_allergy')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('food_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}