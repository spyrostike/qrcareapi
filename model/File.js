const uuid = require('uuidv4').default
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params) => {
        const file = ObjectUtil.generateFileForInsertToDb(params)

        return await con.transaction(trx => {
            con('file')
            .returning('id')
            .insert(file)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getFileById: async (con, params) => {
        const { id, status } = params
        const iStatus = status || 1

        return await con('file')
            .select('*')
            .where('id', id)
            .first()
    }
}