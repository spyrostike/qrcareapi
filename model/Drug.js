const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('drug').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const drug = ObjectUtil.generateDrugForInsertToDb(params, user, number)
   
        return await con.transaction((trx)  => {
            con('drug')
            .transacting(trx)
            .returning('id')
            .insert(drug)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const drug = ObjectUtil.generateDrugForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('drug')
            .update(drug)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getDrugById: async (con, params, callback) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('drug')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getDrugListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ d: 'drug' })
            .select({
                id: 'd.id',
                code: 'd.code',
                number: 'd.number',
                name: 'd.name',
                description: 'd.description',
                member_id: 'd.member_id',
                creature_id: 'd.creature_id',
                drug_image_id: 'd.drug_image_id',
                status: 'd.status',
                create_by: 'd.create_by',
                create_date: 'd.create_date',
                edit_by: 'd.edit_by',
                edit_date: 'd.edit_date',
                drug_image: con.raw('concat( f.path, \'/\', f.name )')
            })
            .leftJoin('file as f', 'd.drug_image_id', 'f.id')
            .where('d.status', iStatus)
            .andWhere('d.creature_id', creature_id)
            .orderBy('d.number', 'asc')
    }, 
    countDrugByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('drug')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('drug')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}