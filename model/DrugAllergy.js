const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('drug_allergy').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const drugAllergy = ObjectUtil.generateDrugAllergyForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('drug_allergy')
            .transacting(trx)
            .returning('id')
            .insert(drugAllergy)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const drugAllergy = ObjectUtil.generateDrugAllergyForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('drug_allergy')
            .update(drugAllergy)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getDrugAllergyById: async (con, params, callback) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('drug_allergy')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getDrugAllergyListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ da: 'drug_allergy' })
            .select({
                id: 'da.id',
                code: 'da.code',
                number: 'da.number',
                name: 'da.name',
                description: 'da.description',
                creature_id: 'da.creature_id',
                description: 'da.description',
                drug_allergy_image_id: 'drug_allergy_image_id',
                member_id: 'da.member_id',
                status: 'da.status',
                drug_allergy_image: con.raw('concat( f.path, \'/\', f.name )'),
                create_date: 'da.create_date',
                create_by: 'da.create_by',
                edit_date: 'da.edit_date',
                edit_by: 'da.edit_by',
            })
            .leftJoin('file as f', 'da.drug_allergy_image_id', 'f.id')
            .where('da.status', iStatus)
            .andWhere('da.creature_id', creature_id)
            .orderBy('da.number', 'asc')

path: "2019111920"
type: "image/jpg"
    },
    countDrugAllergyByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('drug_allergy')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('drug_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}